# Rust Analyzer - Specs

## Rust Editions

### 2015

* [Rust 2015 / 1.0](https://doc.rust-lang.org/edition-guide/rust-2015/index.html)

### 2018

* [Rust 2018 / 1.31.0](https://doc.rust-lang.org/edition-guide/rust-2018/index.html)

### 2021

* [Rust 2021 / 1.56.0](https://doc.rust-lang.org/edition-guide/rust-2021/index.html)

## Nightly Features

* [Rust Unstable Features](https://doc.rust-lang.org/nightly/unstable-book/index.html)
